package uzb.apponlinecredit.payload;

import lombok.Data;

@Data
public class ReqSignIn {
    private String username;
    private String password;
}
