package uzb.apponlinecredit.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uzb.apponlinecredit.entity.Role;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    private UUID id;

    private String firstName;

    private String lastName;

    private String middleName;

    private String phoneNumber;

    private String password;

    private String prePassword;

    private Double salary;

    private String passport;

    private String gender;

    private String givenBy;

    private Date issueDate;

    private Date expireDate;

    private Date birthDate;

    private String birthOfPlace;

    private String nationality;

    private Set<Role> roles;

    public UserDto(UUID id, String firstName, String lastName, String middleName, String phoneNumber, Double salary, String passport, String gender, String givenBy, Date issueDate, Date expireDate, Date birthDate, String birthOfPlace, String nationality) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
        this.passport = passport;
        this.gender = gender;
        this.givenBy = givenBy;
        this.issueDate = issueDate;
        this.expireDate = expireDate;
        this.birthDate = birthDate;
        this.birthOfPlace = birthOfPlace;
        this.nationality = nationality;
    }
}
