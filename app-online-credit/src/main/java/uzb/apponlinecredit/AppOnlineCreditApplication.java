package uzb.apponlinecredit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOnlineCreditApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppOnlineCreditApplication.class, args);
    }

}
