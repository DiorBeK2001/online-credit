package uzb.apponlinecredit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.apponlinecredit.entity.User;
import uzb.apponlinecredit.payload.UserDto;
import uzb.apponlinecredit.repository.UserRepository;

import java.util.Optional;

@Service
public class InfoService {

    @Autowired
    UserRepository userRepository;

    /**
     *User haqida Info
     */
    public UserDto getUserDto(User user){
        Optional<User> optionalUser = userRepository.findById(user.getId());
        if (optionalUser.isPresent()){
            return new UserDto(
                    user.getId(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getMiddleName(),
                    user.getPhoneNumber(),
                    user.getSalary(),
                    user.getPassport(),
                    user.getGender().toString(),
                    user.getGivenBy(),
                    user.getIssueDate(),
                    user.getExpireDate(),
                    user.getBirthDate(),
                    user.getBirthOfPlace(),
                    user.getNationality()
            );
        }
        return new UserDto();
    }
}
