package uzb.apponlinecredit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzb.apponlinecredit.entity.Credit;
import uzb.apponlinecredit.entity.User;
import uzb.apponlinecredit.payload.ApiResponse;
import uzb.apponlinecredit.security.CurrentUser;
import uzb.apponlinecredit.service.CreditGiveService;

@RestController
@RequestMapping("/api/giveCredit")
public class CreditGiveController {

    @Autowired
    CreditGiveService creditGiveService;

    /**
     * User ga credit berish
     */
    @PostMapping
    public HttpEntity<?> giveCredit(@CurrentUser User user, @RequestParam(name = "creditId") Credit credit){
        ApiResponse apiResponse = creditGiveService.giveCredit(user, credit);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
}
