package uzb.apponlinecredit.projection;

import org.springframework.data.rest.core.config.Projection;
import uzb.apponlinecredit.entity.Credit;

@Projection(name = "customCredit", types = Credit.class)
public interface CustomCredit {
    Integer getId();

    String getNameCredit();

    Double getSum();

    double getPercent();
}
