package uzb.apponlinecredit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uzb.apponlinecredit.entity.enums.Gender;
import uzb.apponlinecredit.entity.template.UuidEntity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class User extends UuidEntity implements UserDetails {

    //Ism familiya
    @Column(nullable = false)
    private String firstName, lastName;

    //sharifi
    private String middleName;

    //tel nomer
    @Column(nullable = false)
    private String phoneNumber;

    //paroli
    @JsonIgnore
    @Column(nullable = false)
    private String password;

    //oyligi
    @Column(nullable = false)
    private Double salary;

    //Passport seriya va nomeri
    @Column(nullable = false, unique = true)
    private String passport;

    //Jinsi
    @Enumerated(EnumType.STRING)
    private Gender gender;

    //Kim tomonidan berilgani
    @Column(nullable = false)
    private String givenBy;

    //Berilgan muddati
    @Column(nullable = false)
    private Date issueDate;

    //Tugash muddati
    @Column(nullable = false)
    private Date expireDate;

    //Tug'ilgan sanasi
    @Column(nullable = false)
    private Date birthDate;

    //Qayerda tugilgan
    @Column(nullable = false)
    private String birthOfPlace;

    //Millati
    @Column(nullable = false)
    private String nationality;

    //Role
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    private boolean isAccountNonExpired = true;

    private boolean isAccountNonLocked = true;

    private boolean isCredentialsNonExpired = true;

    private boolean enabled = false;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return getPassport();
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public User(String firstName, String lastName, String middleName, String phoneNumber, String password, Double salary, String passport, Gender gender, String givenBy, Date issueDate, Date expireDate, Date birthDate, String birthOfPlace, String nationality, Set<Role> roles, boolean enabled) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.salary = salary;
        this.passport = passport;
        this.gender = gender;
        this.givenBy = givenBy;
        this.issueDate = issueDate;
        this.expireDate = expireDate;
        this.birthDate = birthDate;
        this.birthOfPlace = birthOfPlace;
        this.nationality = nationality;
        this.roles = roles;
        this.enabled = enabled;
    }
}
