package uzb.apponlinecredit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.apponlinecredit.entity.Role;
import uzb.apponlinecredit.entity.enums.RoleName;

import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Set<Role> findAllByRoleName(RoleName name);
}
