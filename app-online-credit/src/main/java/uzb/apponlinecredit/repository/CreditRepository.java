package uzb.apponlinecredit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uzb.apponlinecredit.entity.Credit;
import uzb.apponlinecredit.projection.CustomCredit;

@RepositoryRestResource(path = "credit", collectionResourceRel = "list", excerptProjection = CustomCredit.class)
public interface CreditRepository extends JpaRepository<Credit, Integer> {

    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    @Override
    <S extends Credit> S save(S s);

    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    @Override
    void deleteById(Integer integer);
}
